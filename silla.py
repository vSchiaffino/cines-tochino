class Silla:
    def __init__(self, fila, columna, estado="libre"):
        self.fila = fila
        self.columna = columna
        self.estado = estado

    def click(self):
        if self.estado == "libre":
            self.estado = "elegida"
        elif self.estado == "elegida":
            self.estado = "libre"