from flask import Flask, redirect, url_for, render_template, session, request
from sala import Sala
from dto.cliente import Cliente
from dao.clienteDAO import loginCliente
from dao.generosDAO import get_carousel_items
from dao.peliculasDAO import get_pelicula_by_id
from dao.funcionesDAO import getDiasFunciones, getHorariosFunciones

app = Flask(__name__)
app.secret_key = "tochidiosovosmugroso"


def existe_esta_key(key):
    try:
        _ = session[key]
        if key is None:
            raise KeyError
        return True
    except KeyError:
        return False


def check_usuario():
    if not existe_esta_key("id"):
        redirect(url_for("login", invalid_login=False))
# ------------- routes normales ------------------
@app.route("/home")
@app.route("/")
def home():
    check_usuario()
    return render_template("home.html")


@app.route("/peliculas")
def peliculas():
    check_usuario()
    generos_con_peliculas = get_carousel_items()
    return render_template("peliculas.html", gcp=generos_con_peliculas)


@app.route("/pelicula/<idpelicula>", methods=["GET", "POST"])
def pelicula(idpelicula):
    check_usuario()
    if request.method == "GET":
        p = get_pelicula_by_id(idpelicula)
        return render_template("detalle.html", p=p)
    if request.method == "POST":
        session["idpeliculaelegida"] = idpelicula
        return redirect(url_for("elegir"))


@app.route("/elegir/", methods=["GET", "POST"])
def elegir():
    check_usuario()
    if request.method == "GET":
        if not existe_esta_key("idpeliculaelegida"):
            # No eligio pelicula
            return redirect(url_for("peliculas"))
        else:
            dias = getDiasFunciones(session["idpeliculaelegida"])
            horarios = ""
            sala = ""
            if existe_esta_key("horario"):
                # TODO cargar salas
                salas = getHorariosFunciones(session["idpeliculaelegida"], session["horario"])
            elif existe_esta_key("sala"):
                # TODO setear sala
                pass
        # ----- listo -----
        return render_template("elegir.html", dias=dias, horarios=horarios, sala=sala)
    elif request.method == "POST":

        f = request.form
        print(f)


@app.route("/busqueda")
def busqueda():
    check_usuario()
    return render_template("busqueda.html")


@app.route("/estrenos")
def estrenos():
    check_usuario()
    return render_template("busqueda.html")


@app.route("/reservas")
def reservas():
    check_usuario()
    return render_template("reservas.html")


@app.route("/perfil")
def perfil():
    check_usuario()
    return render_template("perfil.html")


@app.route("/login", methods=["POST", "GET"])
def login():
    if request.method == "GET":
        try:
            id = session["id"]
            if id == 0:
                raise KeyError
            else:
                return redirect(url_for("home"))
        except KeyError:
            pass
        return render_template("login.html", invalid_login=False)
    elif request.method == "POST":
        usuario = request.form["usuario"]
        contrasena = request.form["contraseña"]
        id = loginCliente(Cliente(usuario=usuario, contraseña=contrasena))
        if id == 0:
            return render_template("login.html", invalid_login=True)
        else:
            session["id"] = id
            return redirect(url_for("home"))


@app.route("/registro")
def registro():
    try:
        id = session["id"]
        if id == 0:
            raise KeyError
        else:
            return redirect(url_for("home"))
    except KeyError:
        pass
    return render_template("registro.html")


# ------------ funciones -------------------
@app.route("/logout")
def logout():
    session.clear()
    return redirect(url_for("login"))


@app.route("/terminar-compra/")
def terminar_compra():
    global salas
    salas.pop(session["id"])
    session.clear()

    # return render_template("compra-success")
    return redirect(url_for("elegir"))


@app.route("/elegir-dia/<dia>")
def elegir_dia(dia):
    session["id"] = 1
    session["dia"] = dia
    return redirect(url_for("elegir"))


@app.route("/elegir-horario/<horario>")
def elegir_horario(horario):
    try:
        # Veo si eligio dia
        dia = session["dia"]
        if dia is None:
            raise KeyError
        else:
            # Si dia esta vacio o no existe tirario KeyError.
            # En caso de que no este vacio se ejecuta esto
            global salas
            session["horario"] = horario
            salas[session["id"]] = Sala()
    except KeyError:
        pass
    # De todos modos lo vuelvo a mandar a elegir
    return redirect(url_for("elegir"))


@app.route("/salaclick/<fila>/<columna>")
def click(fila, columna):
    try:
        global salas
        if session["dia"] is None or session["horario"] is None or salas[session["id"]] is None:
            raise KeyError
        else:
            sala = salas[session["id"]]
            sala.click(int(fila), int(columna))
            salas[session["id"]] = sala
    except KeyError:
        pass
    except TypeError:
        pass
    return redirect(url_for("elegir"))


if __name__ == "__main__":
    app.run(debug=True, host="192.168.0.23", port=5010)
