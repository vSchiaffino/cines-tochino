import mysql.connector
from dto.pelicula_por_genero import Pelicula_para_generos


def getAllGeneros() -> list:
    """
    Devuelve una lista con todos los generos.
    :return: list(tuple(str, int))
    """
    db = mysql.connector.connect(
        host="remotemysql.com",
        user="5BaqbCTtNO",
        passwd="KvGftDfAjb",
        database="5BaqbCTtNO"
    )
    cursor = db.cursor()
    cursor.execute("SELECT * FROM generos")
    lista = []
    rows = cursor.fetchall()
    cursor.close()
    db.close()
    for row in rows:
        lista.append((row[1], row[0]))
    return lista


def get_peliculas_by_genero(idGenero: int) -> list:
    lista = []
    db = mysql.connector.connect(
        host="remotemysql.com",
        user="5BaqbCTtNO",
        passwd="KvGftDfAjb",
        database="5BaqbCTtNO"
    )
    cursor = db.cursor()
    cursor.execute(f"SELECT g.idpelicula, p.rutaimagen FROM"
                   f" generosxpelicula g "
                   f"INNER JOIN peliculas p ON g.idpelicula=p.id "
                   f"WHERE g.idgenero = {idGenero};")
    rows = cursor.fetchall()
    cursor.close()
    db.close()
    for row in rows:
        lista.append(Pelicula_para_generos(row[1], row[0]))
    return lista


def get_carousel_items():
    lista = []
    # lista = [["genero", [peli1, peli2]], ["genero2", [peli3, peli4]]]
    generos = getAllGeneros()
    for genero in generos:
        peliculas = get_peliculas_by_genero(genero[1])
        lista.append([genero[0], peliculas])
 b    return lista