import mysql.connector


def getNextId(tabla: str) -> int:
    """
    Funcion que sirve para obtener el proximo id de una tabla,
    es generico.
    :return:
    :param tabla: str
    :return: int
    """
    db = mysql.connector.connect(
        host="remotemysql.com",
        user="5BaqbCTtNO",
        passwd="KvGftDfAjb",
        database="5BaqbCTtNO"
    )
    cursor = db.cursor()
    cursor.execute("SELECT max(id)+1 FROM " + tabla)
    records = cursor.fetchall()
    cursor.close()
    db.close()
    id = records[0][0]
    if id is None:
        return 1
    return int(id)
