import mysql.connector
from dto.cliente import Cliente
from dao.DAOHelper import getNextId



def altaCliente(c : Cliente) -> bool:
    """
    Da de alta un cliente.
    :param c: Cliente
    :return: bool
    """
    db = mysql.connector.connect(
        host="remotemysql.com",
        user="5BaqbCTtNO",
        passwd="KvGftDfAjb",
        database="5BaqbCTtNO"
    )
    cursor = db.cursor()
    # -----  validaciones  ------------
    lenusuario = len(c.usuario)
    if c.usuario == "":
        raise AssertionError("el usuario no puede estar vacío.")
    if lenusuario < 5 or lenusuario > 50:
        raise AssertionError("el usuario debe ser entre 5 y 50 caracteres.")
    if c.contraseña == "":
        raise AssertionError("la contraseña no puede estar vacía.")
    lc = len(c.contraseña)
    if lc < 4 or lc > 50:
        raise AssertionError("La contraseña debe ser entre 4 y 50 caracteres.")
    # ----- todoo ok -----
    c.id = getNextId("clientes")
    if c.id == 0:
        raise AssertionError("el id no puede ser 0")
    cmdText = f"INSERT INTO clientes(id, usuario, contraseña) VALUES ({c.id}, '{c.usuario}', '{c.contraseña}');"
    try:
        cursor.execute(cmdText)
        db.commit()
        cursor.close()
        db.close()
        return True
    except Exception():
        print(Exception())
        cursor.close()
        return False


def modificarCliente(c : Cliente) -> bool:
    """
    Modifica un cliente, debe tener el id seteado.
    :param c: Cliente
    :return: bool
    """
    db = mysql.connector.connect(
        host="remotemysql.com",
        user="5BaqbCTtNO",
        passwd="KvGftDfAjb",
        database="5BaqbCTtNO"
    )
    cursor = db.cursor()
    # -----  validaciones  ------------
    lenusuario = len(c.usuario)
    if c.usuario == "":
        raise AssertionError("el usuario no puede estar vacío.")
    if lenusuario < 5 or lenusuario > 50:
        raise AssertionError("el usuario debe ser entre 5 y 50 caracteres.")
    if c.contraseña == "":
        raise AssertionError("la contraseña no puede estar vacía.")
    lc = len(c.contraseña)
    if lc < 4 or lc > 50:
        raise AssertionError("La contraseña debe ser entre 4 y 50 caracteres.")
    # ----- todoo ok -----
    if c.id == 0:
        raise AssertionError("el id no puede ser 0")
    cmdText = f"UPDATE clientes SET usuario='{c.usuario}', contraseña='{c.contraseña}' WHERE id = {c.id};"
    try:
        cursor.execute(cmdText)
        db.commit()
        cursor.close()
        db.close()
        return True
    except Exception():
        print(Exception())
        cursor.close()
        db.close()
        return False


def loginCliente(c : Cliente) -> int:
    """
    Intenta loguear un cliente. Si lo logra devuelve el id del cliente que se logueo.
    Caso contrario devuelve 0.
    :param c: Cliente
    :return: int
    """
    db = mysql.connector.connect(
        host="remotemysql.com",
        user="5BaqbCTtNO",
        passwd="KvGftDfAjb",
        database="5BaqbCTtNO"
    )
    cursor = db.cursor()
    cursor.execute(f"SELECT * FROM clientes WHERE usuario='{c.usuario}'")
    rows = cursor.fetchall()
    cursor.close()
    db.close()
    if len(rows) == 0:
        return 0
    else:
        row = rows[0]
        if c.contraseña == str(row[2]):
            c.id = int(row[0])
            return c.id
