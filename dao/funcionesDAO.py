import mysql.connector
from dto.funciones import Funcion


def _ReadAll(w):
    db = mysql.connector.connect(
        host="remotemysql.com",
        user="5BaqbCTtNO",
        passwd="KvGftDfAjb",
        database="5BaqbCTtNO"
    )
    cursor = db.cursor()
    cursor.execute("SELECT f.id,f.idsala,f.horario,f.fecha,f.idpelicula, ff.nombre, f.idformato "
                   "FROM funciones f "
                   "INNER JOIN formatovideo ff "
                   "ON ff.id = f.idformato " + w)
    fs = cursor.fetchall()
    cursor.close()
    db.close()
    funciones = list()
    for f in fs:
        funcion = Funcion(f[0], f[1], f[2], f[3], f[4], f[5], f[6])
        funciones.append(funcion)
    return funciones


def getDiasFunciones(idpelicula):
    funciones = _ReadAll(f"WHERE f.idpelicula = {idpelicula}")
    dias = []
    [dias.append((f.fecha, str(f.fecha))) for f in funciones]
    return dias


def getHorariosFunciones(idpelicula, dia):
    funciones = _ReadAll(f"WHERE f.idpelicula = {idpelicula} AND f.fecha = '{dia}'")
    horarios = []
    [horarios.append(f.horario) for f in funciones]
    return horarios

