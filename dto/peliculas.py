import datetime


class Pelicula:
    def __init__(self, id=0, nombre="", duracion="",
                 idclasificacion=0, rutaimagen="",
                 rutavideo="", estreno=datetime.date(year=1000, month=1, day=1), sinopsis="",
                 idgenero=0, clasificacion="", actores="", directores="", generos=""):
        self.id = id
        self.nombre = nombre
        self.duracion = duracion
        self.idclasificacion = idclasificacion
        self.rutaimagen = rutaimagen
        self.rutavideo = rutavideo
        self.estreno = estreno
        self.sinopsis = sinopsis
        self.idgenero = idgenero
        self.clasificacion = clasificacion
        self.actores = actores
        self.directores = directores
        self.generos = generos


pelicula = Pelicula()

