from silla import Silla

class Sala:
    def __init__(self):
        self.largo = 20
        self.ancho = 20
        self.sillas = []
        for i in range(self.largo):
            fila = []
            for j in range(self.ancho):
                if j == 9:
                    fila.append(Silla(i, j, "ocupada"))
                elif j == 2:
                    fila.append(Silla(i, j, "pasillo"))
                else:
                    fila.append(Silla(i, j))
            self.sillas.append(fila)

    def click(self, fila, columna):
        self.sillas[fila][columna].click()